/**
 * Implement required functions and run with:
 *
 * node prototypes.js
 */

console.info("Initializing prototypes.");

/**
 * Generic book
 * :param name - String
 * :param price - Number
 * :param currency - String
 */
function Product(name, price, currency) {
    this.name = name;
    this.price = price;
    this.currency = currency || "$";
}

Product.prototype.prettyPrice = function() {
    return this.currency + this.price;
};

/**
 * Physical book
 * :param name - String
 * :param price - Number
 * :param nPages - Number (int)
 */
function Book(name, price, nPages) {
    Product.call(this, name, price);
    this.nPages = nPages;
}
Book.prototype = new Product();

/**
 * Return true if the book is longer than 100 pages.
 */
Book.prototype.isLong = function() {
    return this.nPages > 100;
};

/**
 * Digital book
 * :param name - String
 * :param price - Number
 * :param nPages - Number (int)
 * :param fileSize - Number (int) - in bytes
 */
function EBook(name, price, nPages, fileSize) {
    Book.call(this, name, price, nPages);
    this.fileSize = fileSize;
}
EBook.prototype = new Book();

/**
 * Return true if the file is bigger than 8MB.
 */
EBook.prototype.isHeavy = function() {
    return this.fileSize > 8388608;//8388608 sono 8 MB in bytes
};

/**
 * Just a pen.
 * :param name - String
 * :param price - Number
 */
function Pen(name, price) {
    Product.call(this, name, price);
}
Pen.prototype = new Product();

/**
 * Heterogeneous list of products.
 * :param products - Array
 */
function ProductsList(products) {
    this.products = products || [];
}

/**
 * Return the total price.
 */
ProductsList.prototype.getTotalPrice = function() {
    var totalPrice = 0;
    for(i=0; i<this.products.length; i++){
    	totalPrice = totalPrice + this.products[i].price;
    }
    return totalPrice;
};

/**
 * Return a list of pens.
 */
ProductsList.prototype.getPens = function() {
    var penList = [];
    for(i=0; i<this.products.length; i++){
    	if(this.products[i] instanceof Pen) {
    		penList.push(this.products[i]);
    	}
    }
    return penList;
};

/**
 * Return a list of books (and e-books).
 */
ProductsList.prototype.getBooks = function() {
    var bookList = [];
    for(i=0; i<this.products.length; i++){
    	if(this.products[i] instanceof Book) {
    		bookList.push(this.products[i]);
    	}
    }
    return bookList;
};

/**
 * Return a list of e-books.
 */
ProductsList.prototype.getEBooks = function() {
    var ebookList = [];
    for(i=0; i<this.products.length; i++){
    	if(this.products[i] instanceof EBook) {
    		ebookList.push(this.products[i]);
    	}
    }
    return ebookList;
};

/**
 * Return a list of short books (and e-books).
 */

ProductsList.prototype.getShortBooks = function() {
	var shortBook = [];
	for(i=0; i<this.products.length; i++){
		if(this.products[i] instanceof Book && !this.products[i].isLong()) {
    		shortBook.push(this.products[i]);
    	}
   	}
    return shortBook;
};

/**
 * Return a list of heavy e-books.
 */

ProductsList.prototype.getHeavyEBooks = function() {
    var heavyBook = [];
	for(i=0; i<this.products.length; i++){
		if(this.products[i] instanceof EBook && this.products[i].isHeavy()) {
    		heavyBook.push(this.products[i]);
    	}
   	}
    return heavyBook;
};


// real data

console.info("Initializing objects.");

var mylist = new ProductsList([
    new Book("The Little Prince", 10, 70),
    new Book("1984", 15, 300),
    new EBook("The hard thing about hard things", 14, 250, 15728640),  // 15MB
    new EBook("The Elegance of the Hedgehog", 13, 160, 5242880),  // 5MB
    new EBook("Amazon Web Services in Action", 15, 400, 10485760),  // 10MB
    new Pen("Red Pen", 2),
    new Pen("Blue Pen", 1),
]);


// tests

console.info("Executing tests.");

if (mylist.getPens().length !== 2) {
    console.error("Wrong # of Pens:", mylist.getPens().length);
}

if (mylist.getBooks().length !== 5) {
    console.error("Wrong # of Books:", mylist.getBooks().length);
}

if (mylist.getEBooks().length !== 3) {
    console.error("Wrong # of Ebooks:", mylist.getEBooks().length);
}

if (mylist.getShortBooks().length !== 1) {
    console.error("Wrong # of short Books:", mylist.getShortBooks().length);
}

if (mylist.getHeavyEBooks().length !== 2) {
    console.error("Wrong # of heavy E-Books:", mylist.getHeavyEBooks().length);
}

if (mylist.getTotalPrice() !== 70) {
    console.error("Wrong total price:", mylist.getTotalPrice());
}

console.info("Done.");